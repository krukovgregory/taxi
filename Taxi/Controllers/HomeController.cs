﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Taxi.Models;
using System.Data.Entity;
using System.Net;

namespace Taxi.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {                                                           
            if (User.IsInRole("manager"))
            {
                var model1 = db.Requests.Include(s => s.Status).Include(r => r.ApplicationUser).ToList();
                return View("IndexManager",model1);
            }

            if(User.IsInRole("driver"))
            {
                var model2 = db.Requests.
                    Include(s => s.Status).Include(r => r.ApplicationUser).
                    Where(s => s.ApplicationUser.UserName == User.Identity.Name);
                return View("IndexDriver", model2);
            }

            var model = db.Requests.Include(s => s.Status).Include(r => r.ApplicationUser).ToList();
            return View("IndexAnonim",model);
        }


        public ActionResult Users()
        {
            return View("Users");
        }


#region Request
        public ActionResult Create()
        {
            ViewBag.StatusId = new SelectList(db.Status, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,InAddr,OutAddr,InLat,InLng,OutLat,OutLng,Dt,ApplicationUserId,StatusId")] Request request)
        {
            if (ModelState.IsValid)
            {
                request.Id = Guid.NewGuid();
                request.Status = db.Status.Where(c => c.Name == "Новая").First();
                request.Dt = DateTime.Now;
                db.Requests.Add(request);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.ApplicationUserId = new SelectList(db.ApplicationUsers, "Id", "Email", request.ApplicationUserId);
            ViewBag.StatusId = new SelectList(db.Status, "Id", "Name", request.StatusId);
            return View(request);
        }

        public ActionResult CanselRequest(Guid? id)
        {
            var model = db.Requests.Find(id);
            if(model==null)
            {
                return HttpNotFound();            
            }
            model.Status = db.Status.Where(c => c.Name == "Отменена").First();
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Request request = db.Requests.Find(id);
            if (request == null)
            {
                return HttpNotFound();
            }
            ViewBag.ApplicationUser = new SelectList(db.Users, "Id", "Email");
            //ViewBag.StatusId = new SelectList(db.Status, "Id", "Name", request.StatusId);
            return View(request);
        }

        // POST: Requests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,InAddr,OutAddr,InLat,InLng,OutLat,OutLng,Dt,ApplicationUserId,StatusId")] Request request)
        {
            if (ModelState.IsValid)
            {
                request.StatusId = db.Status.Where(c => c.Name == "Назначена").First().Id;

                db.Entry(request).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            // ViewBag.ApplicationUserId = new SelectList(db.ApplicationUsers, "Id", "Email", request.ApplicationUserId);
            ViewBag.StatusId = new SelectList(db.Status, "Id", "Name", request.StatusId);
            return View(request);
        }

        public ActionResult RunRequest(Guid? id)
        {
            var model = db.Requests.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            model.Status = db.Status.Where(c => c.Name == "В работе").First();
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult StropRequest(Guid? id)
        {
            var model = db.Requests.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            model.Status = db.Status.Where(c => c.Name == "Завершена").First();
            db.SaveChanges();
            return RedirectToAction("Index");
        }
#endregion




    }
}