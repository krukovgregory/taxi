﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taxi.Models;

namespace Taxi.Controllers
{
    //[Authorize(Roles = "manager")]
    public class UserController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;

        public UserController()
        {
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
        }

        // GET: ApplicationUsers
        public ActionResult Index()
        {
            var model = db.Users.Where(x=> x.Remove==false).Select(c => new UsersViewModel
            {
                Id = c.Id,
                Email = c.Email,
                Name = c.UserName,
                Role =c.Roles.FirstOrDefault().RoleId
            }).ToList();

            foreach(var buf in model)
            {
                buf.Role = db.Roles.Find(buf.Role).Name;
            }

            return View(model);
        }

        // GET: ApplicationUsers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // GET: ApplicationUsers/Create
        public ActionResult Create()
        {
            var model = new UsersViewModel();
            ViewBag.Role = new SelectList(db.Roles, "Name", "Name");
            return View();
        }

        // POST: ApplicationUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Email,Name,Password,Role")] UsersViewModel model)
        {
            if (ModelState.IsValid)
            {
                var admin = new ApplicationUser { UserName = model.Name, Email = model.Email };

                var user = _userManager.CreateAsync(admin, model.Password).Result;
                var role = _userManager.AddToRoleAsync(admin.Id, model.Role).Result;
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: ApplicationUsers/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }

            var model = new UsersViewModel
                {
                Name =applicationUser.UserName,
                Email =applicationUser.Email,
                Id =applicationUser.Id
                };

            return View(model);
        }

        // POST: ApplicationUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  ActionResult Edit([Bind(Include = "Id,Email,Password,Name")] UsersViewModel model)
        {
            if (ModelState.IsValid)
            {
                _userManager.RemovePasswordAsync(model.Id);
                _userManager.AddPasswordAsync(model.Id, model.Password);
                                             
                var user = db.Users.Find(model.Id);
                user.UserName = model.Name;
                db.SaveChanges();               
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: ApplicationUsers/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);

            if (applicationUser == null || applicationUser.UserName=="admin")
            {
                return HttpNotFound();
            }

            var model = new UsersViewModel
            {
                Name = applicationUser.UserName,
                Id = applicationUser.Id,
                Email = applicationUser.Email
            };

            return View(model);
        }

        // POST: ApplicationUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ApplicationUser applicationUser = db.Users.Find(id);
            var req = db.Requests.Where(c => c.ApplicationUserId == applicationUser.Id).Count();

            if (req == 0)
            {
                db.Users.Remove(applicationUser);
            }
            else
            {
                applicationUser.Remove = true;
            }

            
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
