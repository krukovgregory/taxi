namespace Taxi.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Taxi.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Taxi.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Taxi.Models.ApplicationDbContext context)
        {
            /*������������� ��*/
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            var manager = new IdentityRole { Name = "manager" };
            var driver = new IdentityRole { Name = "driver" };

            roleManager.Create(manager);
            roleManager.Create(driver);

            var admin = new ApplicationUser { UserName = "admin", Email = "admin@taxi.ru" };
            userManager.Create(admin, "Pwd000");            

            userManager.AddToRole(admin.Id,manager.Name);


            context.Status.AddOrUpdate(
                                x=>x.Name,
                                new Status { Name = "�����" },
                                new Status { Name = "��������", Role = manager.Id },
                                new Status { Name = "��������", Role = driver.Id },
                                new Status { Name = "���������", Role = manager.Id },
                                new Status { Name = "� ������", Role = driver.Id },
                                new Status { Name = "���������", Role = driver.Id }
                                );
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
