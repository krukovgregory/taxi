﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Taxi.Models
{
    public class Request
    {
        public Guid Id { get; set; }
        [Required]
        [Display(Name = "Откуда")]
        public String InAddr { get; set; }
        [Required]
        [Display(Name = "Куда")]
        public String OutAddr { get; set; }
        public double? InLat { get; set; }
        public double? InLng { get; set; }
        public double? OutLat { get; set; }
        public double? OutLng { get; set; }
        [DataType(DataType.DateTime)]
        [Display(Name = "Дата время заявки")]
        public DateTime Dt { get; set; }        
        public String ApplicationUserId { get; set; }
        [Display(Name = "Водитель")]
        public ApplicationUser ApplicationUser { get; set; }
        public int StatusId { get; set; }
        [Display(Name = "Статус")]
        public Status Status { get; set; }
    }

    public class Status
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public String Role { get; set; }
        public ICollection<Request> Request { get; set; }
        public Status()
        {
            Request = new List<Request>();
        }

    }

}
