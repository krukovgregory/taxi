﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Taxi.Models
{
    public class UsersViewModel
    {
        public String Id { get; set; }

        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public String Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public String Password { get; set; }

        [Required]
        [Display(Name = "Имя")]
        public String Name { get; set; }

        [Required]
        [Display(Name = "Роль")]
        public String Role { get; set; }
    }

    public class RequestViewModel
    {
        public Guid Id { get; set; }
        [Required]
        [Display(Name = "Откуда")]
        public String InAddr { get; set; }
        [Required]
        [Display(Name = "Куда")]
        public String OutAddr { get; set; }
        public double? InLat { get; set; }
        public double? InLng { get; set; }
        public double? OutLat { get; set; }
        public double? OutLng { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Дата время заявки")]
        public DateTime Dt { get; set; }
        [Display(Name = "Водитель")]
        public ApplicationUser Driver { get; set; }
        [Required]
        [Display(Name = "Статус")]
        public Status Status { get; set; }
    }


}